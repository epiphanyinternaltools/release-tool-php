<?php
namespace Epiphany\Release;

use Epiphany\Release\Process\ProcessBranchStatus;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Tasks\AuthenticateBitbucket;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Epiphany\Release\Tasks\CheckForUpdates;
use Epiphany\Release\Tasks\ParseArguments;
use Epiphany\Release\Tasks\PerformRelease;
use Epiphany\Release\Tasks\PrintReadmeUpdates;
use Epiphany\Release\Tasks\PrintVersionInformation;
use Epiphany\Release\Tasks\PrintWelcomeMessage;
use Epiphany\Release\Tasks\TaskFactory;
use Epiphany\Release\Tasks\updateReadme;
use Epiphany\Release\Tasks\ValidateRequiredFiles;
use Epiphany\Release\Updater\UpdaterFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Epiphany\Release\FileManager;

class ReleaseCommand extends Command
{
    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var TaskFactory
     */
    private $taskFactory;

    /**
     * ReleaseCommand constructor.
     * @param null $name
     * @param TaskFactory $taskFactory
     */
    public function __construct($name = null, TaskFactory $taskFactory)
    {
        $this->taskFactory = $taskFactory;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('run')
            ->addArgument('type', InputArgument::OPTIONAL, 'Specify the release type')
            ->addOption(
                'remote',
                null,
                InputOption::VALUE_OPTIONAL,
                'Please specify the remote',
                'origin'
            )
            ->addOption(
                'branch',
                null,
                InputOption::VALUE_OPTIONAL,
                'Please specify the branch',
                'master'
            )
            ->setDescription('Release a new version and deploy')
            ->setHelp('This command will update the version and deploy the code on the remote repository');
    }


    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
    
        $this->io = new SymfonyStyle($input, $output);

        // List of Tasks what we need to perform
        $tasks = [
            CheckForUpdates::class,
            ValidateRequiredFiles::class,
            PrintWelcomeMessage::class,
            CheckBranchStatus::class,
            ParseArguments::class,
            PrintVersionInformation::class,
            PrintReadmeUpdates::class,
            PerformRelease::class,
        ];

        foreach ($tasks as $task) {
            $taskFactory = $this->taskFactory->build($task, $this->io, $input, $output, $this->getApplication());

            // If any of the tasks fail. Stop Execution
            if (!$taskFactory->handle()) {
                exit;
            }
        }
    }
}
