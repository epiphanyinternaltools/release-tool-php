<?php
/**
 * Created by PhpStorm.
 * User: dimitris.stoikidis
 * Date: 20/04/2017
 * Time: 11:19
 */

namespace Epiphany\Release\Process;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ProcessValidator
{
    public function isValid(Process $process, $type = null)
    {
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        if ($type === 'shouldNotHaveResponse' && $process->getOutput()) {
            return false;
        }

        return true;
    }
}
