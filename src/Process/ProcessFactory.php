<?php
namespace Epiphany\Release\Process;

use Symfony\Component\Process\Process;

class ProcessFactory
{
    /**
     * @param $command
     * @return Process
     */
    public function build($command)
    {
        return new Process($command);
    }
}
