<?php
namespace Epiphany\Release;

use Epiphany\Release\Process\ProcessBranchStatus;
use Epiphany\Release\Tasks\SelfUpdate;
use Epiphany\Release\Tasks\TaskFactory;
use Epiphany\Release\Tasks\updateReadme;
use Epiphany\Release\Updater\UpdaterFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Humbug\SelfUpdate\Updater;

class SelfUpdateCommand extends Command
{
    /**
     * @var TaskFactory
     */
    private $taskFactory;

    /**
     * ReleaseCommand constructor.
     * @param null $name
     * @param TaskFactory $taskFactory
     */
    public function __construct($name = null, TaskFactory $taskFactory)
    {
        $this->taskFactory = $taskFactory;
        parent::__construct($name);
    }
    protected function configure()
    {
        $this->setName('self-update')
            ->addArgument('username', InputArgument::OPTIONAL, 'Specify your Bitbucket username')
            ->addArgument('password', InputArgument::OPTIONAL, 'Specify your Bitbucket password')
            ->setDescription('Self updates the release tool.')
            ->setHelp('Just run: release.phar self-update');
    }

    /**
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
    
        $io = new SymfonyStyle($input, $output);
        $selfUpdateTask = $this->taskFactory->build(SelfUpdate::class, $io, $input, $output);

        try {
            $selfUpdateTask->handle();
        } catch (\Exception $e) {
            $io->error("Well, something happened!\nPlease check your bitbucket credentials and try again");
        }
    }
}
