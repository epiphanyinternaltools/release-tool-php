<?php
namespace Epiphany\Release;

class FileManager
{
    /**
     * @var string
     */
    private $versionFileName = 'version';

    /**
     * @var string
     */
    private $readmeFileName = 'README.md';

    /**
     * Root directory
     * @var string
     */
    protected $root;

    /**
     * Updated Version from release
     * @var string
     */
    private $updatedVersion;

    /**
     * FileManager constructor.
     * @param $root
     */
    public function __construct($root)
    {
        $this->setRoot($root);
    }

    /**
     * Set the root directory
     *
     * @param $root
     */
    public function setRoot($root = __DIR__)
    {
        $this->root = $root;
    }

    /**
     * Fetch a local file
     *
     * @param $file
     * @return string
     */
    public function fileContents($file)
    {
        $path = $this->getPath($file);
        return file_get_contents($path);
    }

    /**
     * Build the path to the file
     *
     * @param $file
     * @return string
     */
    protected function getPath($file)
    {
        return $this->root . '/' . $file;
    }

    /**
     * Returns the ReadmeFilename
     *
     * @return string
     */
    public function getReadmeFileName()
    {
        return $this->readmeFileName;
    }
    public function getReadmeFileNameWithPath()
    {
        return $this->getPath($this->readmeFileName);
    }


    /**
     * Returns the version file name
     *
     * @return string
     */
    public function getVersionFileName()
    {
        return $this->versionFileName;
    }


    /**
     * Returns the current version
     *
     * @return mixed
     */
    public function getCurrentVersion()
    {
        return trim(str_replace("VERSION=", "", $this->fileContents($this->versionFileName)));
    }

    /**
     * Updates the version
     *
     * @param $type
     * @return string
     */
    public function getUpdatedVersion($type)
    {
        $versionArray = explode('.', $this->getCurrentVersion());

        $versionsArrayWithKeys = [
            'major' => $versionArray[0],
            'minor' => $versionArray[1],
            'patch' => $versionArray[2],
        ];


        $versionsArrayWithKeys[$type] = $versionsArrayWithKeys[$type] + 1;

        // For minor and major versions, we need to set patch version to 0
        if($type === "major"){
            $versionsArrayWithKeys['minor'] = 0;
            $versionsArrayWithKeys['patch'] = 0;
        }

        if($type === "minor"){
            $versionsArrayWithKeys['patch'] = 0;
        }

        $this->updatedVersion = implode(".", $versionsArrayWithKeys);

        return $this->updatedVersion;
    }

    /**
     * Does the given file exist?
     *
     * @param $file
     * @return bool
     */
    public function fileExists($file)
    {
        return file_exists($this->getPath($file));
    }

    /*
     * Updates the Version file to the updated version
     */
    public function updateVersionFile()
    {
        $content = "VERSION=".$this->updatedVersion;
        $this->setFileContents($this->versionFileName, $content);
    }

    /*
     * Updates Readme.md file
     * with the new version history
     */
    public function updateReadmeFile($content)
    {
        return $this->setFileContents($this->readmeFileName, $content);
    }

    /**
     * Adds content to a file
     *
     * @param $file
     * @param $content
     * @return bool|int
     */
    public function setFileContents($file, $content)
    {
        $path = $this->getPath($file);
        return file_put_contents($path, $content);
    }
}
