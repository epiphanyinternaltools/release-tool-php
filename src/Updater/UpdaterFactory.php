<?php
namespace Epiphany\Release\Updater;

use Humbug\SelfUpdate\Updater;

class UpdaterFactory
{
    /**
     * @var string
     */
    private $repoUrl = 'https://bitbucket.org/epiphanyinternaltools/release-tool-php/raw/master/';

    /**
     * @var string
     */
    private $pharVersionFileName = 'release.phar.version?rand=';

    /**
     * @var
     */
    private $pharFile;

    /**
     * UpdaterFactory constructor.
     *
     * @param $pharFile
     */
    public function __construct($pharFile)
    {
        $this->pharFile = $pharFile;
    }

    public function build()
    {

        $updater = new Updater($this->pharFile, false);
        $updater->getStrategy()->setPharUrl($this->repoUrl . 'release.phar?rand='.rand());
        $updater->getStrategy()->setVersionUrl($this->repoUrl . $this->pharVersionFileName . rand());

        return $updater;
    }
}
