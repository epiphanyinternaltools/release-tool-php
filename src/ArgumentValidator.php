<?php
namespace Epiphany\Release;

class ArgumentValidator
{
    /**
     * Available Release Types
     *
     * @var array
     */
    private $availableTypes = ['patch', 'major', 'minor', 'failfix'];

    /**
     * Returns true if the user specified type
     * is in the list of available types
     *
     * @param $type
     * @return bool
     */
    public function isValid($type)
    {
        if (!in_array($type, $this->availableTypes)) {
            return false;
        }
        return true;
    }
}
