<?php

namespace Epiphany\Release\Tasks;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class PerformRelease extends AbstractTask
{
    public function handle()
    {
        $remote = $this->input->getOption('remote');
        $branch = $this->input->getOption('branch');
        $type = $this->input->getArgument('type');
        $releaseCommands = [
            'git add README.md',
            'git add version',
            'git commit -m "Released version '. $this->fileManager->getUpdatedVersion($type) .'"',
            "git push $remote $branch"
        ];

        $performRelease = $this->io->confirm('Perform Release?', true);
        if ($performRelease) {
            $this->fileManager->updateVersionFile();
            $this->performRelease($releaseCommands);
        }
        return true;
    }

    private function performRelease($releaseCommands)
    {
        foreach ($releaseCommands as $releaseCommand) {
            $process = $this->processFactory->build($releaseCommand);
            $process->run();
            if (!$this->processValidator->isValid($process)) {
                return false;
            }
        }
        return true;
    }
}
