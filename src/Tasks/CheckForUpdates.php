<?php
namespace Epiphany\Release\Tasks;

use Herrera\Phar\Update\Update;

class CheckForUpdates extends AbstractTask
{
    /**
     *
     * @return bool
     */
    public function handle()
    {
	try {    
	    if ($this->updater->hasUpdate()) {
	        $this->io->section("There is a newer version of the release tool.");
	        $update = $this->io->confirm("Would you like to update?", true);
	        if ($update) {
	    	    $command = $this->application->find('self-update');
		    $command->run($this->input, $this->output);
		    return false;
		}
	    }
	}
	catch(\Exception $e) {
	    $this->io->caution('There was a problem checking for the latest version: ' . $e->getMessage());
	}
        return true;
    }
}
