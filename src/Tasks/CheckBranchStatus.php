<?php
namespace Epiphany\Release\Tasks;

use Symfony\Component\Process\Exception\ProcessFailedException;

class CheckBranchStatus extends AbstractTask
{
    /**
     * Task to check the current branch status
     * We need to validate that all changes in files are
     * committed before performing a release
     *
     * @return bool
     */
    public function handle()
    {
        $process = $this->processFactory->build('git status --porcelain');
        $process->run();

        // Check if Process is valid
        $branchIsValid = $this->processValidator->isValid($process, 'shouldNotHaveResponse');
        if (!$branchIsValid) {
            $this->io->error("Release failed. Please make sure that you have committed all your changes.");
        }

        return $branchIsValid;
    }
}
