<?php
namespace Epiphany\Release\Tasks;

use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Updater\UpdaterFactory;
use Humbug\SelfUpdate\Updater;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class TaskFactory
{
    private $processFactory;
    private $argumentValidator;
    private $processValidator;
    private $updater;
    private $fileManager;
    private $application;

    public function __construct(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    ) {
    
        $this->processFactory = $processFactory;
        $this->fileManager = $fileManager;
        $this->argumentValidator = $argumentValidator;
        $this->processValidator = $processValidator;
        $this->updater = $updater;
        $this->application = $application;
    }

    public function build($taskClass, SymfonyStyle $io, $input, $output)
    {
        return new $taskClass(
            $this->processFactory,
                $this->fileManager,
                $this->argumentValidator,
                $io,
                $input,
                $output,
                $this->processValidator,
                $this->updater,
                $this->application
        );
    }
}
