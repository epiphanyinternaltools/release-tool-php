<?php
namespace Epiphany\Release\Tasks;

class ValidateRequiredFiles extends AbstractTask
{
    public function handle()
    {
        $requiredFiles = [
            $this->fileManager->getReadmeFileName(),
            $this->fileManager->getVersionFileName()
        ];

        foreach ($requiredFiles as $requiredFile) {
            if (!$this->fileManager->fileExists($requiredFile)) {
                $this->io->error("$requiredFile file was not detected on your project root folder");
                $create = $this->io->confirm("Would you like me to create a $requiredFile file for you?", true);

                if (!$create) {
                    $this->io->error("Please create the $requiredFile file and run the release tool again");
                    return false;
                }

                if ($this->generateFile($requiredFile)) {
                    $this->processFactory->build("git add $requiredFile")->run();
                    $this->processFactory->build("git commit -m 'Add required $requiredFile file'")->run();
                }
            }
        }

        return true;
    }

    /**
     * @param $requiredFile
     * @return bool|int
     */
    public function generateFile($requiredFile)
    {
        if ($requiredFile === 'version') {
            return file_put_contents($requiredFile, "VERSION=1.0.0");
        }
        return touch($requiredFile);
    }
}
