<?php
namespace Epiphany\Release\Tasks;

class ParseArguments extends AbstractTask
{
    /**
     * Will Check if the 'type' argument was set
     * and if not it will prompt and ask for it
     *
     * @return bool
     */
    public function handle()
    {
        $type = $this->input->getArgument('type');
        if (!$type) {
            $type = $this->io->ask('Please specify the type of release you want to perform', 'patch');
        }
        $typeIsValid = $this->argumentValidator->isValid($type);
        if (!$typeIsValid) {
            $this->io->error('The specified type ' . $type . ' is not valid');
            return false;
        }
        $this->input->setArgument('type', $type);

        return true;
    }
}
