<?php
namespace Epiphany\Release\Tasks;

class SelfUpdate extends AbstractTask
{
    /**
     * Run self-update
     */
    public function handle()
    {
        $result = $this->updater->update();
        if ($result) {
            $this->io->success('Release tool was updated to the latest version.');
        } else {
            $this->io->warning('No update required.');
        }
    }
}
