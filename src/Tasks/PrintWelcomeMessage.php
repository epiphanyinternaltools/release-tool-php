<?php

namespace Epiphany\Release\Tasks;

/**
 * Class PrintWelcomeMessage
 * @package Epiphany\Release\Tasks
 */
class PrintWelcomeMessage extends AbstractTask
{
    /**
     * Prints a Welcome Message
     */
    public function handle()
    {
        $this->io->title('Epiphany Release Tool');
        $this->io->text('Used to make software releases:');
        $this->io->listing(array(
            'Bumps version number according to server standard ({major}.{minor}.{patch})',
            'Updates release notes with recent commit history',
            'Pushes specified branch',
        ));

        return true;
    }
}
