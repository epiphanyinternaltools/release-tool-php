<?php
namespace Epiphany\Release\Tasks;

use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Updater\UpdaterFactory;
use Humbug\SelfUpdate\Updater;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractTask
{
    protected $processFactory;
    protected $fileManager;
    protected $argumentValidator;
    protected $input;
    protected $output;
    protected $processValidator;
    protected $updater;
    protected $application;
    protected $io;

    public function __construct(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        SymfonyStyle $io,
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    ) {
    
        $this->processFactory = $processFactory;
        $this->fileManager = $fileManager;
        $this->argumentValidator = $argumentValidator;
        $this->io = $io;
        $this->input = $input;
        $this->output = $output;
        $this->processValidator = $processValidator;
        $this->updater = $updater;
        $this->application= $application;
    }

    abstract public function handle();
}
