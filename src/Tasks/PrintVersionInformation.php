<?php
/**
 * Created by PhpStorm.
 * User: dimitris.stoikidis
 * Date: 20/04/2017
 * Time: 10:12
 */

namespace Epiphany\Release\Tasks;

class PrintVersionInformation extends AbstractTask
{
    public function handle()
    {
        $type = $this->input->getArgument('type');
        $this->io->listing(array(
            'Current version is <info>' . $this->fileManager->getCurrentVersion() . '</info>',
            'Performing a <info>' . $type . '</info> release'
        ));
        $newVersion = $type == 'failfix' ? $this->fileManager->getCurrentVersion() : $this->fileManager->getUpdatedVersion($type);
        $this->io->note('New version will be: ' . $newVersion);

        return true;
    }
}
