<?php
namespace Epiphany\Release\Tasks;

class PrintReadmeUpdates extends AbstractTask
{
    const VERSION_HISTORY = 'Version History';
    const RELEASED_VERSION = 'Released version ';

    /**
     * Will display the README.me updates
     * and will ask the user to apply them on the README.md file
     *
     * @return bool
     */
    public function handle()
    {
        $this->io->section("README.md will be updated with the following entries:");

        $latestCommits = $this->getCommitsFromLastRelease();
        if (!$latestCommits) {
            $this->io->error('No commits since last release - check you are in the branch and merge as necessary.');
            return false;
        }

        $readmeUpdates = $this->getUpdates($latestCommits);

        $this->io->text($readmeUpdates);
        $this->io->newLine(2);

        $updateReadme = $this->io->confirm('Automatically update readme.MD?', true);
        if ($updateReadme && $this->addCommitsToReadme($readmeUpdates)) {
            return true;
        }

        return false;
    }

    /**
     * Will give a list with all the commits from
     * the last release
     *
     * @return array|bool
     */
    protected function getCommitsFromLastRelease()
    {
        $branch = $this->input->getOption('branch');
        $process = $this->processFactory->build('git log ' . $branch . ' --pretty=format:"[%ad]%s"');
        $process->run();

        if (!$this->processValidator->isValid($process)) {
            return false;
        }

        $commits = explode("\n", $process->getOutput());

        return $this->getCommitMessages($commits);
    }

    /**
     * Returns an array of the commit messages
     * without the date information
     *
     * @param $commits
     * @return array
     */
    protected function getCommitMessages($commits)
    {
        $versionCommitMessage = self::RELEASED_VERSION;
        $commitMessages = [];

        foreach ($commits as $commit) {
            // We found the previous commit
            if (strpos($commit, $versionCommitMessage) !== false) {
                break;
            }

            $commitMessages[] = "\t - " . preg_replace('/\[.*\]/', '', $commit) . PHP_EOL;
        }
        
        return $commitMessages;
    }

    /**
     * Returns an array with the text updates
     * that will be applied to README.md file
     *
     * @param $commitMessages
     * @return array
     */
    private function getUpdates($commitMessages)
    {
        $lines = [];
        $date = date("d F Y", time());
        $lines[] = '##v'. $this->fileManager->getUpdatedVersion($this->input->getArgument('type')) . " - " . $date . PHP_EOL;
        foreach ($commitMessages as $commit) {
            $lines[] = $commit . PHP_EOL;
        }
        return $lines;
    }

    /**
     * Add the commit history to the README.md file
     *
     * @param $readmeUpdates
     * @return bool|int
     */
    protected function addCommitsToReadme($readmeUpdates)
    {
        $lineNumberFoundAt = $this->getVersionHistoryLineNumber();

        if (!$lineNumberFoundAt) {
            $appendVersion = $this
                ->io
                ->confirm("'Version History' was not found on README file. Would you like to add it now?", true);
            if ($appendVersion) {
                array_unshift(
                    $readmeUpdates,
                    PHP_EOL. PHP_EOL."Version History".PHP_EOL."============".PHP_EOL.PHP_EOL
                );
            }
        }
        $contentToAdd = $this->getNewContent($lineNumberFoundAt, $readmeUpdates);

        return $this->fileManager->updateReadmeFile(implode('', $contentToAdd));
    }

    /**
     * Get the content that needs to be added
     *
     * @param $lineNumberFoundAt
     * @param $readmeUpdates
     * @return array
     */
    private function getNewContent($lineNumberFoundAt, $readmeUpdates)
    {
        $fileContents = new \SplFileObject($this->fileManager->getReadmeFileNameWithPath());
        $new_contents = array();
        foreach ($fileContents as $key => $value) {
            $new_contents[] = $value;
            if ($key == $lineNumberFoundAt) {
                foreach ($readmeUpdates as $readmeUpdate) {
                    $new_contents[] = $readmeUpdate;
                }
            }
        }
        return $new_contents;
    }

    /**
     * Get the line that the 'Version History' was found
     *
     * @return int|string
     */
    public function getVersionHistoryLineNumber()
    {
        $fileContents = new \SplFileObject($this->fileManager->getReadmeFileNameWithPath());
        foreach ($fileContents as $lineNumber => $lineContent) {
            if (strpos(strtolower(trim($lineContent)), strtolower(self::VERSION_HISTORY)) !== false) {
                return $lineNumber + 2;
            }
        }
    }
}
