<?php

namespace spec\Epiphany\Release\Process;

use Epiphany\Release\Process\ProcessFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Process\Process;

class ProcessFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ProcessFactory::class);
    }

    function it_returns_a_process()
    {
        $this->build('ls -al')->shouldReturnAnInstanceOf(Process::class);
    }
}
