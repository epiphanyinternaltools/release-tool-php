<?php

namespace spec\Epiphany\Release;

use Epiphany\Release\ArgumentValidator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ArgumentValidatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ArgumentValidator::class);
    }

    function it_should_return_type_if_availiable()
    {
        $this->isValid('patch')->shouldReturn(true);
    }

    function it_should_return_false_if_not_availiable()
    {
        $this->isValid('patchWhatever')->shouldReturn(false);
    }
}
