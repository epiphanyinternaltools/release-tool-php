<?php

namespace spec\Epiphany\Release\Tasks;

use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Epiphany\Release\Tasks\ParseArguments;
use Epiphany\Release\Tasks\PerformRelease;
use Epiphany\Release\Tasks\PrintReadmeUpdates;
use Epiphany\Release\Tasks\PrintVersionInformation;
use Epiphany\Release\Tasks\PrintWelcomeMessage;
use Epiphany\Release\Tasks\TaskFactory;
use Humbug\SelfUpdate\Updater;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Style\SymfonyStyle;

class TaskFactorySpec extends ObjectBehavior
{
    function let($processFactory,$fileManager,$argumentValidator,$processValidator,$updater,$application)
    {
        $processFactory->beADoubleOf(ProcessFactory::class);
        $fileManager->beADoubleOf(FileManager::class);
        $argumentValidator->beADoubleOf(ArgumentValidator::class);
        $processValidator->beADoubleOf(ProcessValidator::class);
        $updater->beADoubleOf(Updater::class);
        $application->beADoubleOf(Application::class);

        $this->beConstructedWith($processFactory,$fileManager,$argumentValidator,$processValidator,$updater,$application);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TaskFactory::class);
    }

    function it_returns_a_task_class($style,$input,$output)
    {

        $style->beADoubleOf(SymfonyStyle::class);
        $input->beADoubleOf('\Symfony\Component\Console\Input\InputInterface');
        $output->beADoubleOf('\Symfony\Component\Console\Output\OutputInterface');


        $tasks = [
            PrintWelcomeMessage::class,
            CheckBranchStatus::class,
            ParseArguments::class,
            PrintVersionInformation::class,
            PrintReadmeUpdates::class,
            PerformRelease::class,
        ];

        foreach ($tasks as $task) {
            $this->build($task, $style, $input, $output)->shouldReturnAnInstanceOf($task);
        }

    }
}
