<?php

namespace spec\Epiphany\Release\Tasks;

use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Humbug\SelfUpdate\Updater;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class CheckBranchStatusSpec extends ObjectBehavior
{
    function let(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        SymfonyStyle $io ,
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    )
    {
        $this->beConstructedWith($processFactory,$fileManager,$argumentValidator,$io, $input, $output, $processValidator, $updater, $application);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CheckBranchStatus::class);
    }

    function it_should_check_branch_status(ProcessFactory $processFactory, Process $process, ProcessValidator $processValidator, SymfonyStyle $io)
    {
        $processFactory->build('git status --porcelain')->willReturn($process);
        $processValidator->isValid($process,'shouldNotHaveResponse')->willReturn(true);
        $process->run()->shouldBeCalled();
        $io->error()->shouldNotBeCalled();
        $this->handle()->shouldReturn(true);
    }

    function it_should_check_branch_status_uncommitted(
        ProcessFactory $processFactory,
        Process $process,
        ProcessValidator $processValidator,
        SymfonyStyle $io
    )
    {
        $processFactory->build('git status --porcelain')->willReturn($process);
        $processValidator->isValid($process,'shouldNotHaveResponse')->willReturn(false);
        $process->run()->shouldBeCalled();
        $io->error("Release failed. Please make sure that you have committed all your changes.")->shouldBeCalled();
        $this->handle()->shouldReturn(false);
    }
}
