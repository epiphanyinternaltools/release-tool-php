<?php

namespace spec\Epiphany\Release\Tasks;

use Epiphany\Release\Tasks\ParseArguments;
use Epiphany\Release\Tasks\PrintWelcomeMessage;
use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Humbug\SelfUpdate\Updater;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use Prophecy\Argument;

class ParseArgumentsSpec extends ObjectBehavior
{
    function let(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        SymfonyStyle $io ,
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    )
    {
        $this->beConstructedWith($processFactory,$fileManager,$argumentValidator,$io, $input, $output, $processValidator, $updater, $application);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ParseArguments::class);
    }

    function it_has_default_argument(InputInterface $input, ArgumentValidator $argumentValidator)
    {
        $input->getArgument('type')->willReturn('patch');
        $argumentValidator->isValid("patch")->willReturn(true);
        $input->setArgument('type','patch')->shouldBeCalled();
        $this->handle()->shouldReturn(true);
    }

    function it_does_not_have_default_argument(InputInterface $input, ArgumentValidator $argumentValidator)
    {
        $input->getArgument('type')->willReturn(null);
        $argumentValidator->isValid(null)->willReturn(false);
        $input->setArgument('type','patch')->shouldNotBeCalled();
        $this->handle()->shouldReturn(false);
    }

    function it_does_not_have_default_argument_and_user_gives_wrong(InputInterface $input, ArgumentValidator $argumentValidator, SymfonyStyle $io)
    {
        $input->getArgument('type')->willReturn(null);
        $io->ask('Please specify the type of release you want to perform', 'patch')->willReturn('whatever');
        $argumentValidator->isValid('whatever')->willReturn(false);
        $io->error('The specified type whatever is not valid')->shouldBeCalled();
        $input->setArgument('type','patch')->shouldNotBeCalled();
        $this->handle()->shouldReturn(false);
    }

    function it_does_not_have_default_argument_and_user_gives_correct(InputInterface $input, ArgumentValidator $argumentValidator, SymfonyStyle $io)
    {
        $input->getArgument('type')->willReturn(null);
        $io->ask('Please specify the type of release you want to perform', 'patch')->willReturn('major');
        $argumentValidator->isValid('major')->willReturn(true);
        $input->setArgument('type','major')->shouldBeCalled();
        $this->handle()->shouldReturn(true);
    }
}
