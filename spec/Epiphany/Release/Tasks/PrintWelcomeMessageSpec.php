<?php

namespace spec\Epiphany\Release\Tasks;

use Epiphany\Release\Tasks\PrintWelcomeMessage;
use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Humbug\SelfUpdate\Updater;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use Prophecy\Argument;

class PrintWelcomeMessageSpec extends ObjectBehavior
{
    function let(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        SymfonyStyle $io ,
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    )
    {
        $this->beConstructedWith($processFactory,$fileManager,$argumentValidator,$io, $input, $output, $processValidator, $updater, $application);
    }
    function it_is_initializable()
    {
        $this->shouldHaveType(PrintWelcomeMessage::class);
    }

    function it_prints_welcome_message(SymfonyStyle $io, FileManager $fileManager)
    {
        $fileManager->getCurrentVersion()->willReturn('1.1.1');
        $io->title("Epiphany Release Tool")->shouldBeCalled();
        $io->text('Used to make software releases:')->shouldBeCalled();
        $io->listing(array(
            'Bumps version number according to server standard ({major}.{minor}.{patch})',
            'Updates release notes with recent commit history',
            'Pushes specified branch',
        ))->shouldBeCalled();
        $this->handle()->shouldReturn(true);
    }

}
