<?php

namespace spec\Epiphany\Release\Tasks;

use Epiphany\Release\Tasks\PrintReadmeUpdates;
use Epiphany\Release\Tasks\PrintVersionInformation;
use Epiphany\Release\Tasks\PrintWelcomeMessage;
use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Humbug\SelfUpdate\Updater;
use phpDocumentor\Reflection\File;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\StyleInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use Prophecy\Argument;

class PrintReadmeUpdatesSpec extends ObjectBehavior
{
    function let(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        SymfonyStyle $io ,
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    )
    {
        $this->beConstructedWith($processFactory,$fileManager,$argumentValidator,$io, $input, $output, $processValidator, $updater, $application);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(PrintReadmeUpdates::class);
    }

    function it_will_print_the_readme_updates(SymfonyStyle $io, Process $process, ProcessFactory $processFactory,InputInterface $input, ProcessValidator $processValidator, FileManager $fileManager)
    {

        $input->getOption('branch')->willReturn('master');
        $input->getArgument('type')->willReturn('patch');
        $processFactory->build('git log master --pretty=format:"[%ad]%s"')->willReturn($process);
        $process->run()->shouldBeCalled();

        $process->getOutput()->willReturn("[Thu Apr 20 12:17:45 2017 +0100]Fix typo in version release check");

        $processValidator->isValid($process)->willReturn(true);

        $fileManager->getCurrentVersion()->willReturn('1.1.1');
        $fileManager->getUpdatedVersion('patch')->willReturn('1.1.2');

        $io->section(Argument::any())->shouldBeCalled();

        $io->text(Argument::containing('##v1.1.2 - ' . date("d F Y",time()) . PHP_EOL))->shouldBeCalled();
        $io->newLine(2)->shouldBeCalled();

        $io->confirm(Argument::any(), true)->shouldBeCalled()->willReturn(true);

        $fileManager->getReadmeFileNameWithPath()->willReturn('README.md');
        $fileManager->updateReadmeFile(Argument::any())->willReturn(100);

        $this->handle()->shouldReturn(true);
    }

    function it_will_fail_when_there_are_no_commit_messages(SymfonyStyle $io, Process $process, ProcessFactory $processFactory,InputInterface $input, ProcessValidator $processValidator, FileManager $fileManager)
    {
        $input->getArgument('type')->willReturn('patch');
        $input->getOption('branch')->willReturn('master');
        $processFactory->build('git log master --pretty=format:"[%ad]%s"')->willReturn($process);
        $process->run()->shouldBeCalled();
        $io->section(Argument::any())->shouldBeCalled();

        // There are no commit messages returned form the process
        $process->getOutput()->willReturn(null);

        $io->error(Argument::any())->shouldBeCalled();
        $this->handle()->shouldReturn(false);
    }


    function it_will_print_the_readme_updates_from_last_release(SymfonyStyle $io, Process $process, ProcessFactory $processFactory,InputInterface $input, ProcessValidator $processValidator, FileManager $fileManager)
    {
        $input->getArgument('type')->willReturn('patch');
        $input->getOption('branch')->willReturn('master');
        $processFactory->build('git log master --pretty=format:"[%ad]%s"')->willReturn($process);
        $process->run()->shouldBeCalled();

        $commitMessages =
        "[Thu Apr 20 12:17:45 2017 +0100]Fix typo in version release check \n 
        [Thu Apr 20 12:17:45 2017 +0100]This is another test commit message \n 
        [Thu Apr 20 12:17:45 2017 +0100]Released version 1.1.2 \n
        [Thu Apr 20 12:17:45 2017 +0100]This message should not be on the update\n";

        $process->getOutput()->willReturn($commitMessages);

        $commitBeforeCurrentRelease = "\t - " . "This message should not be on the update" . "\r" . "\r";
        $processValidator->isValid($process)->willReturn(true);

        $fileManager->getCurrentVersion()->willReturn('1.1.1');
        $fileManager->getUpdatedVersion('patch')->willReturn('1.1.2');

        $io->section(Argument::any())->shouldBeCalled();

        $io->text(Argument::containing('##v1.1.2 - ' . date("d F Y",time()) . PHP_EOL))->shouldBeCalled();
        $io->text(Argument::containing($commitBeforeCurrentRelease))->shouldNotBeCalled();
        $io->newLine(2)->shouldBeCalled();

        $io->confirm(Argument::any(), true)->shouldBeCalled()->willReturn(true);

        $fileManager->getReadmeFileNameWithPath()->willReturn('README.md');
        $fileManager->updateReadmeFile(Argument::any())->willReturn(100);

        $this->handle()->shouldReturn(true);
    }
}
