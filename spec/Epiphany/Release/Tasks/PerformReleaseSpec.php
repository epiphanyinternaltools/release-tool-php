<?php

namespace spec\Epiphany\Release\Tasks;

use Epiphany\Release\Tasks\PerformRelease;
use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Humbug\SelfUpdate\Updater;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class PerformReleaseSpec extends ObjectBehavior
{
    function let(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        SymfonyStyle $io ,
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    )
    {
        $this->beConstructedWith($processFactory,$fileManager,$argumentValidator,$io, $input, $output, $processValidator, $updater, $application);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(PerformRelease::class);
    }

    function it_performs_release(InputInterface $input, FileManager $fileManager, ProcessFactory $processFactory, Process $process, SymfonyStyle $io, ProcessValidator $processValidator)
    {
        $fileManager->getCurrentVersion()->willReturn('1.1.1');

        $input->getArgument('type')->willReturn('patch');
        $input->getOption('remote')->willReturn('origin');
        $input->getOption('branch')->willReturn('master');
        $fileManager->getUpdatedVersion('patch')->willReturn('1.1.2');
        $io->confirm(Argument::any(), true)->shouldBeCalled()->willReturn(true);

        $fileManager->updateVersionFile()->willReturn(200);

        $releaseCommands = [
            'git add README.md',
            'git add version',
            'git commit -m "Released version 1.1.2"',
            "git push origin master"
        ];

        foreach ($releaseCommands as $releaseCommand) {
            $processFactory->build($releaseCommand)->willReturn($process);
            $process->run()->shouldBeCalled();
            $processValidator->isValid($process)->willReturn(true);
        }

        $this->handle()->shouldReturn(true);
    }



}
