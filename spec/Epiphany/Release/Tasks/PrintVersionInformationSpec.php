<?php

namespace spec\Epiphany\Release\Tasks;

use Epiphany\Release\Tasks\PrintVersionInformation;
use Epiphany\Release\Tasks\PrintWelcomeMessage;
use Epiphany\Release\ArgumentValidator;
use Epiphany\Release\FileManager;
use Epiphany\Release\Process\ProcessFactory;
use Epiphany\Release\Process\ProcessValidator;
use Epiphany\Release\Tasks\CheckBranchStatus;
use Humbug\SelfUpdate\Updater;
use phpDocumentor\Reflection\File;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\StyleInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use Prophecy\Argument;

class PrintVersionInformationSpec extends ObjectBehavior
{
    function let(
        ProcessFactory $processFactory,
        FileManager $fileManager,
        ArgumentValidator $argumentValidator,
        SymfonyStyle $io ,
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output,
        ProcessValidator $processValidator,
        Updater $updater,
        Application $application
    )
    {
        $this->beConstructedWith($processFactory,$fileManager,$argumentValidator,$io, $input, $output, $processValidator, $updater, $application);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(PrintVersionInformation::class);
    }

    function it_prints_version_correct_information(InputInterface $input, StyleInterface $io, FileManager $fileManager)
    {
        $fileManager->getCurrentVersion()->willReturn('1.1.1');
        $input->getArgument('type')->willReturn('patch');
        $io->listing(array(
            'Current version is <info>1.1.1</info>',
            'Performing a <info>patch</info> release'
        ))->shouldBeCalled();

        $fileManager->getUpdatedVersion('patch')->shouldBeCalled()->willReturn('1.1.2');
        $io->note('New version will be: 1.1.2')->shouldBeCalled();
        $this->handle()->shouldReturn(true);
    }

    function it_prints_version_correct_information_for_failfix(InputInterface $input, StyleInterface $io, FileManager $fileManager)
    {
        $fileManager->getCurrentVersion()->willReturn('1.1.1');
        $input->getArgument('type')->willReturn('failfix');
        $io->listing(array(
            'Current version is <info>1.1.1</info>',
            'Performing a <info>failfix</info> release'
        ))->shouldBeCalled();

        $fileManager->getUpdatedVersion('failfix')->shouldNotBeCalled();
        $io->note('New version will be: 1.1.1')->shouldBeCalled();
        $this->handle()->shouldReturn(true);
    }

}
