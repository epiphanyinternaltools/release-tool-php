<?php

namespace spec\Epiphany\Release;
//use org\bovigo\vfs\VfsStream;



use Epiphany\Release\FileManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FileManagerSpecbk extends ObjectBehavior
{
    function let()
    {
        VfsStre
        $reflector = new \ReflectionClass("\org\bovigo\vfs\VfsStream");
        $fn = $reflector->getFileName();


        echo dirname($fn);exit;
        VfsStream::setup('root_dir', null, [
            'version' => 'VERSION=3.1.1',
            'README.md' => 'Readme contents'
        ]);

        $this->beConstructedWith(VfsStream::url('root_dir'));
    }

    function it_determines_whether_a_given_file_exists()
    {
        $this->fileExists('version')->shouldBe(true);
        $this->fileExists('README.md')->shouldBe(true);

        unlink(VfsStream::url('root_dir/version'));
        unlink(VfsStream::url('root_dir/README.md'));

        $this->fileExists('version')->shouldBe(false);
        $this->fileExists('README.md')->shouldBe(false);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(FileManager::class);
    }

    function it_gives_the_correct_current_version()
    {
        $this->getCurrentVersion()->shouldReturn("3.1.1");
    }

    function it_gives_the_correct_updated_version_on_patch()
    {
        $this->getUpdatedVersion('patch')->shouldReturn("3.1.2");
    }

    function it_gives_the_correct_updated_version_on_major()
    {
        $this->getUpdatedVersion('major')->shouldReturn("4.1.1");
    }

    function it_gives_the_correct_updated_version_on_minor()
    {
        $this->getUpdatedVersion('minor')->shouldReturn("3.2.1");
    }

    function it_updates_the_readme_file()
    {
        $this->updateReadmeFile("some new content")->shouldBeInteger();
        $this->fileContents('README.md')->shouldMatch('/some new content/');
    }

    function it_updates_the_file_contents()
    {
        $this->setFileContents('README.md',"some new content")->shouldNotReturn(null);
        $this->setFileContents('version',"some new content")->shouldNotReturn(null);
        $this->fileContents('README.md')->shouldMatch('/some new content/');
        $this->fileContents('version')->shouldMatch('/some new content/');
    }

    function it_can_read_file_contents()
    {
        $this->fileContents('README.md')->shouldBe('Readme contents');
        $this->fileContents('version')->shouldBe('VERSION=3.1.1');
    }
}
