Epiphany Release Tool
=====================

Used to make software releases:

- Bumps version number according to semver standard ({major}.{minor}.{patch})
- Updates release notes with recent commit history
- Pushes master branch

### Installation

#### Download Phar File
```
wget https://bitbucket.org/epiphanyinternaltools/release-tool-php/raw/master/release.phar
```
or
```
curl -O https://bitbucket.org/epiphanyinternaltools/release-tool-php/raw/master/release.phar
```

Now you can make the phar-file executable:
```
chmod +x ./release.phar
```
#### Installing it globally
You can place the RElease PHAR anywhere you wish. 
If you put it in a directory that is part of your PATH, 
you can access it globally. On unixy systems you can 
even make it executable and invoke it without directly 
using the php interpreter.

```
mv release.phar /usr/local/bin/release
```
Version History
===============

##v1.0.17 - 27 June 2019
	 - fix to not die when the latest version check fails and update url to check because the bitbucket api has changed

	 - Update Phar & release version file

##v1.0.16 - 23 May 2017
	 - update spec to reflect change to PerformRelease task

##v1.0.15 - 23 May 2017
	 - changed quoting on commit command from single to double quotes as it was not working properly on windows

	 - Update Phar & release version file

	 - Merge branch 'master' of bitbucket.org:epiphanyinternaltools/release-tool-php

##v1.0.14 - 08 May 2017
	 - Fix issue for major releases

##v1.0.13 - 08 May 2017
	 - Fix issue with major/minor versions

	 - Update Phar & release version file

##v1.0.12 - 02 May 2017
	 - Rename Filemanage spec so it will not fail

	 - Fix config error

	 - Update config

	 - Update Phar & release version file

##v1.0.11 - 02 May 2017
	 - Bump up version

##v1.0.4 - 02 May 2017
	 - Merge branch 'master' of bitbucket.org:epiphanyinternaltools/release-tool-php

##v1.0.3 - 02 May 2017
	 - Update scrutinizer configuration

	 - Add missing member variable

	 - Update Phar & release version file

	 - Merge branch 'master' of bitbucket.org:epiphanyinternaltools/release-tool-php

	 - Update scrutinizer configuration

	 - Update Phar & release version file

	 - Update scrutinizer configuration

	 - Update release phar & version file

##v1.0.2 - 27 April 2017
	 - Add phpcs & phpcbf to the project

	 - Apply PHP Code sniffer recomendations

	 - Merge branch 'master' of bitbucket.org:epiphanyinternaltools/release-tool-php

	 - Fixes from phpcbf

	 - Add php code sniffer

	 - README.md edited online with Bitbucket

	 - Add an empty file tp help scrutinizer recognise the project

	 - Update release file & version file

##v1.0.1 - 27 April 2017
	 - Updates version history check

	 - Fixes welcome messages & specs

	 - Add scrutinizer config

	 - Merge branch 'master' of bitbucket.org:epiphanyinternaltools/release-tool-php

	 - Update phar file

	 - README.md edited online with Bitbucket

	 - Update version

	 - Update release phar

	 - Update phar file

	 - Udate phar file and version

	 - Update check

	 - Update readme file

	 - New version

	 - Update phar file & version

	 - Add comment to release new version for testing

	 - Update Release Phar file & Version

	 - Fix version on commit to get the updated version

##v1.0.0 - 25 April 2017
	 - Create empty README.md & version files if missing

	 - Initial commit